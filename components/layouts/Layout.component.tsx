import Head from 'next/head'
import React, { PropsWithChildren } from 'react'
import { Navbar } from '../ui'

type Props = {
    children: React.ReactNode
}

export const Layout:React.FC<PropsWithChildren<Props>> = ({ children }) => {
  return (
    <>
    <Head>
    </Head>
    <Navbar />
    <main style={{ padding: '20px 50px' }}>
        { children }
    </main>
    </>
  )
}
