import type { NextPage } from 'next'
import { Layout } from '../components/layouts'

const Home: NextPage = () => {
  return (
    <Layout>
      <h2>Cookie master</h2>
    </Layout>
  )
}

export default Home
