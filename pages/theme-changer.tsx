import { ChangeEvent, useState, useEffect } from 'react';
import { useRouter } from 'next/router'
import { Card, CardContent, FormControl, FormControlLabel, FormLabel, Radio, RadioGroup } from "@mui/material";
import { Layout } from "../components/layouts";
import Cookies from "js-cookie";
interface Props {
    theme: string;
}

const ThemeChangerPage: React.FC<Props> = () => {
    const [currentTheme, setCurrentTheme] = useState('');
    const router = useRouter();

    useEffect(() => {
        const cookieTheme = Cookies.get('theme') || 'light'
        setCurrentTheme(cookieTheme);
    }, [])
    
    const handleThemeChange = (event: ChangeEvent<HTMLInputElement>) => {
        const selectedTheme = event.target.value;
        setCurrentTheme(selectedTheme);
        Cookies.set('theme', selectedTheme);
        router.reload()
    }

    return (
    <Layout>
        <Card>
            <CardContent>
                <FormControl>
                    <FormLabel>Theme</FormLabel>
                    <RadioGroup
                        value={currentTheme}
                        onChange={handleThemeChange}
                    >
                        <FormControlLabel 
                            value='light'
                            label='Light' 
                            control={<Radio/>}
                        />
                        <FormControlLabel 
                            value='dark'
                            label='Dark' 
                            control={<Radio/>}
                        />
                        <FormControlLabel 
                            value='custom'
                            label='Custom' 
                            control={<Radio/>}
                        />
                    </RadioGroup>
                </FormControl>
            </CardContent>
        </Card>
    </Layout>
  )
}

export default ThemeChangerPage;
